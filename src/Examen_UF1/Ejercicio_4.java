package Examen_UF1;

import java.util.Scanner;

public class Ejercicio_4 {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);

        int i, j=0;
        int [] array = new int[8];
        int [] resultado = new int[10];
        int [] posicion = new int[2];
        int [] valor = new int[2];

        for (i=0;i<array.length;i++){
            System.out.println("Introduce el número " + (i+1));
            while (!lector.hasNextInt()){
                System.out.println("Ese valor no es posible, vuelve a introducirlo");
                lector.next();
            }
            array[i]=lector.nextInt();
        }

        for (i=0;i<valor.length;i++){
            System.out.println("Introduce el valor adicional " + (i+1));
            while (!lector.hasNextInt()){
                System.out.println("Ese valor no es posible, vuelve a introducirlo");
                lector.next();
            }
            valor[i]=lector.nextInt();

            System.out.println("Introduce la posición que desea dentro del array");
            while (!lector.hasNextInt()){
                System.out.println("Ese valor no es posible, vuelve a introducirlo");
                lector.next();
            }
            posicion[i]=lector.nextInt();
            while (posicion[i]<0 || posicion[i]>9){
                System.out.println("Eso no es posible, solo funciona de las posiciones 0 a 9, vuelve a introducirlo");
                while (!lector.hasNextInt()){
                    System.out.println("Ese valor no es posible, vuelve a introducirlo");
                    lector.next();
                }
                posicion[i]=lector.nextInt();
            }

        }

        for (i=0;i<resultado.length;i++){

            if (posicion[0]==i){
                resultado[i]=valor[0];
                j--;
            }else if (posicion[1]==i){
                resultado[i]=valor[1];
                j--;
            }else {
                resultado[i]=array[j];
            }
            j++;
        }

        for (int lista: resultado){
            System.out.print(lista + " ");
        }


    }
}
