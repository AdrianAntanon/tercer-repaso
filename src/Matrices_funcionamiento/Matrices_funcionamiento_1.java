package Matrices_funcionamiento;
/*
@Version: 10/01/2020
@Author: Adrian Antanon
@Description: A program that shows you different ways to display/process the data in a matrix.
 */
public class Matrices_funcionamiento_1 {
    public static void main(String[] args) {

        int [][] matriz = {
                {3, 4, 5},
                {6, 7, 8},
                {9, 10, 11}
        };

        int i, j;

        System.out.println("Versión A: mostrar los valores de 1-3, 2-2 y 3-1");

        System.out.println(matriz[0][2] + " - " + matriz[1][1] + " - " + matriz[2][0]);

        System.out.println("Versión B: recorrer los valores de la matriz y mostrarlos por filas");
        for (i=0;i<matriz.length;i++){
            System.out.print("{ ");
            for (j=0;j<matriz[i].length;j++){
                System.out.print(matriz[i][j]+" ");
            }
            System.out.println("}");
        }

        System.out.println("Versión C: mostrarlo por columnas en vez de por filas");

        for (i=0;i<matriz.length;i++){
            System.out.print("{ ");
            for (j=0;j<matriz[i].length;j++){
                System.out.print(matriz[j][i] + " ");
            }
            System.out.println("}");
        }

        System.out.println("Versión D: mostrar los valores que están en diagonal");
        System.out.print("{ ");
        for (i=0;i<matriz.length;i++){
            for (j=0;j<matriz[i].length;j++){
                if (i == j){
                    System.out.print(matriz[i][j]+" ");
                }
            }
        }
        System.out.println("}");
    }
}
