package Matrices_funcionamiento;

/*
@Version: 10/01/2020
@Author: Adrian Antanon
@Description:A program that asks you a matrix, then it shows you transposed matrix, after that ends with a matrix of the subtraction between the matrix normal and transposed.
 */

import java.util.Scanner;

public class Matrices_funcionamiento_4 {
    public static void main(String[] args) {
        Scanner lector = new Scanner (System.in);

        int i, j;
        int[][] matriz = new int[3][3];
        int[][] transpuesta = new int[3][3];
        int[][] resta = new int[3][3];

        for (i=0;i<matriz.length;i++){
            for (j=0;j<matriz[i].length;j++){
                System.out.println("Introduce el valor " + (i+1)+"-"+(j+1)+", por favor");
                while (!lector.hasNextInt()){
                    System.out.println("Eso no es un número entero, vuelve a introducirlo, por favor");
                    lector.next();
                }
                matriz[i][j]=lector.nextInt();
            }
        }

        System.out.println("Matriz normal: ");

        for (i=0;i<matriz.length;i++){
            System.out.print("{ ");
            for (j=0;j<matriz[i].length;j++){
                System.out.print(matriz[i][j] + " ");
            }
            System.out.println("}");
        }

        System.out.println("Matriz transpuesta: ");

        for (i=0;i<matriz.length;i++){
            System.out.print("{ ");
            for (j=0;j<matriz[i].length;j++){
                transpuesta[i][j] = matriz[j][i];
                System.out.print(transpuesta[i][j] + " ");
            }
            System.out.println("}");
        }

        System.out.println("Matriz con la resta de ambas matrices");

        for (i=0;i<matriz.length;i++){
            System.out.print("{ ");
            for (j=0;j<matriz[i].length;j++){
                resta[i][j] = matriz[i][j] - transpuesta[i][j];
                System.out.print(resta[i][j] + " ");
            }
            System.out.println("}");
        }
    }
}
