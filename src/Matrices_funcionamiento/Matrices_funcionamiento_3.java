package Matrices_funcionamiento;

import java.util.Scanner;
/*
@Version: 10/01/2020
@Author: Adrian Antanon
@Description: A program that declares 4 types of matrix, first, values multiple of 5 until fill it, after that 2 matrix introduced by keyboard and finally the sum between the second and third matrix.
 */
public class Matrices_funcionamiento_3 {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);

        int i, j, contador=0;
        int [][] primero = new int[4][3];
        int [][] segundo = new int[4][2];
        int [][] tercero =  new int[4][2];
        int [][] suma = new int[4][2];

        for (i=0;i<segundo.length;i++){
            for (j=0;j<segundo[i].length;j++){
                System.out.println("Introduce el valor de " + (i+1) + "-" + (j+1) + ", por favor");
                while (!lector.hasNextInt()){
                    System.out.println("Eso no es un valor válido, vuelve a introducirlo, por favor");
                    lector.next();
                }
                segundo[i][j]=lector.nextInt();
            }
        }

        for (i=0;i<tercero.length;i++){
            for (j=0;j<tercero[i].length;j++){
                System.out.println("Introduce el valor de " + (i+1) + "-" + (j+1) + ", por favor");
                while (!lector.hasNextInt()){
                    System.out.println("Eso no es un valor válido, vuelve a introducirlo, por favor");
                    lector.next();
                }
                tercero[i][j]=lector.nextInt();
            }
        }

        System.out.println("Matriz 4x3 con la tabla del 5");
        for (i=0;i<primero.length;i++){
            System.out.print("{ ");
            for (j=0;j<primero[i].length;j++){
                contador++;
                primero[i][j] = contador*5;
                System.out.print(primero[i][j] + " ");
            }
            System.out.println("}");
        }
        System.out.println("Primera matriz: ");
        for (i=0;i<segundo.length;i++){
            System.out.print("{ ");
            for (j=0;j<segundo[i].length;j++){
                System.out.print(segundo[i][j] + " ");
            }
            System.out.println("}");
        }
        System.out.println("Segunda matriz: ");
        for (i=0;i<tercero.length;i++){
            System.out.print("{ ");
            for (j=0;j<tercero[i].length;j++){
                System.out.print(tercero[i][j] + " ");
            }
            System.out.println("}");
        }

        System.out.println("Matriz de la suma de los valores entre la primera y segunda matriz ");
        for (i=0;i<segundo.length;i++){
            System.out.print("{ ");
            for (j=0;j<segundo[i].length;j++){
                suma[i][j] = segundo[i][j] + tercero[i][j];
                System.out.print(suma[i][j] + " ");
            }
            System.out.println("}");
        }
    }
}
