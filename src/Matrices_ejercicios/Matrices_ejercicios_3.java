package Matrices_ejercicios;

import java.text.DecimalFormat;

/*
@Version: 09/01/2020
@Author: Adrian Antanon
@Description: A program that shows you a matrix of 10x5 floats that contains the grade of asignatures by each student.
 */

public class Matrices_ejercicios_3 {
    public static void main(String[] args) {

        DecimalFormat decimales = new DecimalFormat("#0.00");

        int i=0, j=0;
        float[][] notas = new float[10][5];
        char[][] nombres = {
                {'J','u','a','n'},
                {'C','a','r','l','o','s'},
                {'A','d','r','i','á','n'},
                {'A','l','e','x'},
                {'J','a','v','i','e','r'},
                {'H','e','c','t','o','r'},
                {'F','r','a','n'},
                {'B','i','b','i'},
                {'M','a','r'},
                {'J','e','n','n','i'}
        };

        for (i=0;i<=5;i++){
            if (i == 0){
                System.out.print("Nombre alumno         ");
            }
            else {
                System.out.print("Asig " + i +"    ");
            }
        }

        System.out.println(" ");

        for (i=0;i<notas.length;i++){

            for (j=0;j<nombres[i].length;j++){
                System.out.print(nombres[i][j]);
            }

            if(nombres[i].length == 4){
                System.out.print("  ");
            }else if(nombres[i].length == 3){
                System.out.print("   ");
            }else if(nombres[i].length == 5){
                System.out.print(" ");
            }
            System.out.print("                 ");

            for (j=0;j<notas[i].length;j++){
                notas[i][j]= (float) (Math.random()*10);
                if (notas[i][j]==10){
                    System.out.print(decimales.format(notas[i][j]) + "     ");
                }else{
                    System.out.print(decimales.format(notas[i][j]) + "      ");
                }
            }
            System.out.println(" ");
        }
    }
}
