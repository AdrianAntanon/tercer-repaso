package Matrices_ejercicios;

/*
@Version: 10/01/2020
@Author: Adrian Antanon
@Description: A program that asks you two matrix 3x3 by keyboard and fills another matrix with the sum between each other.
 */

import java.util.Scanner;

public class Matrices_ejercicios_7 {
    public static void main(String[] args) {

        Scanner lector = new Scanner(System.in);

        int [][] primera = new int[3][3];
        int[][] segunda = new int[3][3];
        int [][] suma = new int[3][3];
        int i, j;

        for (i=0;i<primera.length;i++){
            for (j=0;j<primera[i].length;j++){
                System.out.println("Introduce el número " + (i+1) + "-"+ (j+1)+" de la primera matriz, por favor");
                while(!lector.hasNextInt()){
                    System.out.println("Eso no es un número entero, vuelve a introducirlo, por favor");
                    lector.next();
                }
                primera[i][j]=lector.nextInt();
            }
        }

        for (i=0;i<segunda.length;i++){
            for (j=0;j<segunda[i].length;j++){
                System.out.println("Introduce el número " + (i+1) + "-"+ (j+1)+" de la segunda matriz, por favor");
                while(!lector.hasNextInt()){
                    System.out.println("Eso no es un número entero, vuelve a introducirlo, por favor");
                    lector.next();
                }
                segunda[i][j]=lector.nextInt();
            }
        }

        System.out.println("La primera matriz es:");

        for (i=0;i<suma.length;i++){
            System.out.print("{ ");
            for (j=0;j<suma[i].length;j++){
                System.out.print(primera[i][j] + " ");
            }
            System.out.println("}");
        }

        System.out.println("\n" +
                "La segunda matriz es: ");

        for (i=0;i<suma.length;i++){
            System.out.print("{ ");
            for (j=0;j<suma[i].length;j++){
                System.out.print(segunda[i][j] + " ");
            }
            System.out.println("}");
        }

        System.out.println("\n" +
                "La matriz formada por la suma de los valores de ambas sería la siguiente:");

        for (i=0;i<suma.length;i++){
            System.out.print("{ ");
            for (j=0;j<suma[i].length;j++){
                suma[i][j] = primera[i][j] + segunda[i][j];
                System.out.print(suma[i][j] + " ");
            }
            System.out.println("}");
        }
    }
}
