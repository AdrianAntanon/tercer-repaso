package Matrices_ejercicios;

import java.util.Scanner;
/*
@Version: 10/01/2020
@Author: Adrian Antanon
@Description: A program that asks you the number of products in storage on differents warehouse and shows you the total amount of each product.
 */
public class Matrices_ejercicios_10 {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);

        int [][] almacen = new int[4][3];
        String [] productos = {"Iphone 4S", "Ipad 2", "Ipad 23G"};
        int [] stock = new int[3];
        int i, j;

        for (i=0;i<almacen.length;i++){
            for (j=0;j<almacen[i].length;j++){
                System.out.println("Introduce la cantidad de "+productos[j] + " del almacén " + (i+1)+", por favor");
                while (!lector.hasNextInt()){
                    System.out.println("La cantidad no es correcta, vuelve a introducirlo.");
                    lector.next();
                }
                almacen[i][j] = lector.nextInt();
                while (almacen[i][j] < 0){
                    System.out.println("Es imposible tener stock negativo, vuelve a introducirlo, por favor");
                    while (!lector.hasNextInt()){
                        System.out.println("La cantidad no es correcta, vuelve a introducirlo.");
                        lector.next();
                    }
                    almacen[i][j] = lector.nextInt();
                }
            }
        }

        for (i=0;i<almacen.length;i++){
            for (j=0;j<almacen[i].length;j++){
                stock[j] = almacen[i][j] + stock[j];
            }
        }

        System.out.println("\n" +
                "Total stock\n" +
                "***********");
        for (i=0;i<stock.length;i++){
            System.out.println(productos[i] + " - " + stock[i]);
        }

    }
}
