package Matrices_ejercicios;

import java.util.Scanner;

/*
@Version: 09/01/2020
@Author: Adrian Antanon
@Description: A program that asks you 5 phrases and saves it on matrix of 5x100 of chars, finally shows you the phrase and the amount of letters that you used on each.
 */

public class Matrices_ejercicios_2 {
    public static void main(String[] args) {

        Scanner lector = new Scanner (System.in);

        String frase = "";
        char [][] palabras = new char[5][100];
        char letra;
        int i, j, contador;
        int [] array = new int[5];
        String [] frases = new String[5];

        for (i=0;i<palabras.length;i++){
            System.out.println("Introduce la frase " + (i+1)+", por favor");
            frase = lector.nextLine();
            contador=0;
            frases[i]=frase;
            for (j=0;j<palabras[i].length;j++){

                if (j < frase.length()){
                    letra = frase.charAt(j);
                    palabras[i][j] = letra;
                    if (palabras[i][j] != ' '){
                        contador++;
                    }
                }
            }
            array[i] = contador;
        }

        for (i=0;i<array.length;i++){
            System.out.println("La frase --> " + frases[i] + " <-- tiene " + array[i] + " letras.");
        }

    }
}
