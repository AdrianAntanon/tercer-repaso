package Matrices_ejercicios;

import java.util.Scanner;

/*
@Version: 09/01/2020
@Author: Adrian Antanon
@Description: A program that counts every vocal of a phrase introduced by keyboard.
 */

public class Matrices_ejercicios_4 {
    public static void main(String[] args) {

        Scanner lector = new Scanner(System.in);

        System.out.println("Introduce una frase, por favor");
        String frase = lector.nextLine();

        char letra;
        char[] letras = {'a','e','i','o','u'};
        char [][] palabras = new char[5][100];
        int [] vector = new int[5];
        int i, j, contador=0;

        System.out.println("La frase para analizar es --> " + frase + " <--");

        for (i=0;i<palabras.length;i++){
            contador=0;
            for (j=0;j<frase.length();j++){
                letra=frase.charAt(j);
                palabras[i][j] = letra;
                if (letras[i] ==  palabras[i][j]){
                    contador++;
                }
            }
            vector[i]=contador;

            if (vector[i] == 0){
                System.out.println("La letra " +letras[i] + " no aparece ninguna vez");
            }else if (vector[i] == 1){
                System.out.println("La letra " +letras[i] + " aparece " + vector[i] + " vez");
            } else{
                System.out.println("La letra " +letras[i] + " aparece " + vector[i] + " veces");
            }

        }



    }
}
