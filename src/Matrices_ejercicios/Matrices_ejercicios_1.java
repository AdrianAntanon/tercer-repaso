package Matrices_ejercicios;

/*
@Version: 09/01/2020
@Author: Adrian Antanon
@Description: A program that counts since 1 until 100 and it saves in a matrix 10 x 10 each value.
 */

public class Matrices_ejercicios_1 {
    public static void main(String[] args) {

        int i, j, contador = 0;
        int [][] matriz = new int[10][10];

        for (i=0;i<matriz.length;i++){
            System.out.print("[ ");
            for (j=0;j<matriz[i].length;j++){
                contador++;
                matriz[i][j] = contador;
                System.out.print(matriz[i][j] + " ");
            }
            System.out.println("]");
        }

    }
}
