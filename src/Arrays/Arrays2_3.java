package Arrays;

public class Arrays2_3 {
    public static void main(String[] args) {
        int i, j=0, z=0;
        int [] array1 = {1,2,3,4,5,6,7,8};
        int [] array2 = {9, 10, 11, 12, 13, 14, 15, 16};
        int [] array3 = new int[16];

        for (i=0;i<array3.length;i++){

            if (i%2==0){
                array3[i]=array1[j];
                j++;
            }else {
                array3[i]=array2[z];
                z++;
            }

        }

        System.out.println("El array con los valores combinados sería el siguiente: ");

        for (int lista: array3){
            System.out.print(lista + " ");
        }

    }
}
