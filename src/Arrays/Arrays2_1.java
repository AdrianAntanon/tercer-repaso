package Arrays;

public class Arrays2_1 {
    public static void main(String[] args) {

        String frase = "Hola mundo";
        int i, contador = 0;
        char letra;
        char [] letras = frase.toCharArray();

        for (i=0;i<letras.length;i++){

            letra = frase.charAt(i);

            if (letra != ' '){
                contador++;
            }

        }

        System.out.println("La frase " + frase + " está compuesta por " + contador + " letras.");


    }
}
