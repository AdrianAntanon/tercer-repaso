package Arrays;

import java.util.Scanner;

public class Arrays1_1 {
    public static void main(String[] args) {

        int i, buscar = 7, contador = 0, max=0, min=0;
        int[]array = {1, 2, 3, 4, 5, 6, 7, 8, 20, -5};

        for (i=0;i<array.length;i++){

            if(i==0){
                max=min=array[i];
            }

            if (array[i] == buscar){
                contador++;
            }

            if (max < array[i]){
                max=array[i];
            }
            if (min > array[i]){
                min=array[i];
            }

        }

        if (contador == 0){
            System.out.println("El número no aparece ninguna vez");
        }else if (contador == 1){
            System.out.println("El número aparece 1 vez");
        }else{
            System.out.println("El número aparece " + contador + " veces");
        }

        System.out.println("El valor mínimo es " + min + " y el valor máximo es " + max);

    }
}
