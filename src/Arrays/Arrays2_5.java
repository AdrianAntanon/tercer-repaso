package Arrays;

import java.util.Scanner;

public class Arrays2_5 {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);

        int i;
        int [] array = new int[20];

        for (i=0;i<array.length;i++){
            System.out.println("Introduce el valor " +(i+1) + ", por favor");
            while (!lector.hasNextInt()){
                System.out.println("Eso no es un número entero, vuelve a introducirlo");
                lector.next();
            }
            array[i]=lector.nextInt();
        }

        for (int lista: array){
            System.out.print(lista + " ");
        }

    }
}
