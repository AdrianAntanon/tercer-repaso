package Arrays;

public class Arrays1_3 {
    public static void main(String[] args) {

        int i, buscar=7, mayor=0, menor=0, igual=0;
        int [] array = {1, 5, 10, 7, -25, 958, 7, 42, 38, 0};

        for (i=0;i<array.length;i++){

            if (buscar==array[i]){
                igual++;
            }else if (buscar>array[i]){
                mayor++;
            }else{
                menor++;
            }

        }

        System.out.println("El número a buscar es " + buscar + " y es igual a " + igual + " números, mayor que " + mayor + " y más pequeño que " +menor);
    }
}
