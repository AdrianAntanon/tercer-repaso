package Arrays;

public class Arrays2_2 {
    public static void main(String[] args) {

        int i;
        int array[] = {1, 12, 4, 5, 7, 3, 2, 9, 6, 7};

        for (i=0;i<array.length;i++){

            if (i == 0){
                System.out.println("Los números múltiples de 3 dentro del array son los siguientes");
            }

            if (array[i]%3==0){
                System.out.print(array[i]+" ");
            }

        }
    }
}
