package Arrays;

public class Arrays1_4 {
    public static void main(String[] args) {

        int i, suma=0;
        int [] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        for (i=0;i<array.length;i++){

            if (i%3==0){
                suma=array[i]+suma;
            }

        }

        for(int lista: array){
            System.out.print(lista + " ");
        }

        System.out.println("\n" +
                "La suma de las posiciones dentro del vector múltiples de 3 es " + suma);

    }
}
