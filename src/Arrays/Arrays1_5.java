package Arrays;

public class Arrays1_5 {
    public static void main(String[] args) {

        int i;
        int [] array = new int[6];

        for (i=0;i<array.length;i++){
            array[i]= (int) (Math.random()*50);
        }

        System.out.println("Los números introducidos dentro del array son: ");

        for (int lista: array){
            System.out.print(lista + " ");
        }

    }
}
