package Arrays;

import java.util.Scanner;

public class Arrays1_2 {
    public static void main(String[] args) {

        Scanner lector = new Scanner(System.in);

        int i;
        int [] array = new int[10];

        for (i=0;i<array.length;i++){
            System.out.println("Introduce el número " + (i+1) + ", por favor");
            while (!lector.hasNextInt()){
                System.out.println("No es un número entero, vuelve a introducirlo, por favor");
                lector.next();
            }
            array[i]=lector.nextInt();
            while (array[i]<0 || array[i]>20){
                System.out.println("El número debe estar comprendido entre 0 y 20.");
                while (!lector.hasNextInt()){
                    System.out.println("No es un número entero, vuelve a introducirlo, por favor");
                    lector.next();
                }
                array[i]=lector.nextInt();
            }
        }

        System.out.println("El array quedaría de la siguiente forma: ");
        for (i=0;i<array.length;i++){
            System.out.print(array[i] + " ");
        }

    }
}
