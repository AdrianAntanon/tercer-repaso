package Arrays;

import java.util.Scanner;

public class Arrays2_4 {
    public static void main(String[] args) {
        Scanner lector = new Scanner (System.in);

        int i, j;
        int [] par = new int[20];

        System.out.println("Qué valor le gustaría dar a la I, debe ser par");

        while (!lector.hasNextInt()){
            System.out.println("Debe ser un número entero");
            lector.next();
        }

        i=lector.nextInt();
        while (i%2!=0){
            System.out.println("El número debe ser par, vuelva a introducirlo");
            while (!lector.hasNextInt()){
                System.out.println("Debe ser un número entero");
                lector.next();
            }
            i=lector.nextInt();
        }

        for (j=0;j<par.length;j++){

            par[j]=i;
            i+=2;

        }

        System.out.println("Usando el valor introducido nos da lo siguiente");

        for (int lista: par){
            System.out.print(lista + " ");
        }
    }
}
